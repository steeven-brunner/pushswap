/*
** function_sort.c for sort1 in /home/brunne_s/rendu/CPE_2013_Pushswap
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Thu Dec 19 17:40:06 2013 brunner steeven
** Last update Mon Jan  6 23:34:54 2014 brunner steeven
*/

#include "linked_list.h"
#include <stdlib.h>

void	sa(t_l_a *l_a)
{
  int	stock;

  stock = l_a->val;
  l_a->val = l_a->next->val;
  l_a->next->val = stock;
}

void	sb(t_l_a *l_a)
{
  int	stock;

  stock = l_a->val;
  l_a->val = l_a->next->val;
  l_a->next->val = stock;
}

void	ss(t_l_a *l_a)
{
  sa(l_a);
  sb(l_a);
}
