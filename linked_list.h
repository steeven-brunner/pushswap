/*
** linked_list.h for linked_list in /home/brunne_s/rendu/CPE_2013_Pushswap
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Thu Dec 19 18:00:26 2013 brunner steeven
** Last update Fri Dec 20 16:35:27 2013 brunner steeven
*/

#ifndef LINKED_LIST_H_
# define LINKED_LIST_H_

typedef struct	s_l_a
{
  int		val;
  struct s_l_a	*next;
}	      	t_l_a;

#endif /* !LINKED_LIST_H_ */
