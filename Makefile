##
## Makefile for lib in /home/brunne_s/rendu/Piscine-C-lib/my
## 
## Made by brunner steeven
## Login   <brunne_s@epitech.net>
## 
## Started on  Mon Oct 21 10:03:07 2013 brunner steeven
## Last update Mon Jan  6 23:31:37 2014 brunner steeven
##

NAME =	push_swap

RM = rm -f

SRC =	function_sort.c \
	pushswap.c \

CC =	gcc

OBJ =	$(SRC:.c=.o)

all :
	$(CC) $(SRC) -o $(NAME)
clean :
	$(RM) $(OBJS)

fclean : clean
	$(RM) $(NAME)
