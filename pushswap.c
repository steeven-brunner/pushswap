/*
** pushswap.c for pushswap in /home/brunne_s/rendu/CPE_2013_Pushswap
** 
** Made by brunner steeven
** Login   <brunne_s@epitech.net>
** 
** Started on  Thu Dec 19 16:56:05 2013 brunner steeven
** Last update Mon Jan  6 23:34:34 2014 brunner steeven
*/

#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

void	my_put_in_list(t_l_a **l_a, int nbr)
{
  t_l_a	*elem;

  elem = malloc(sizeof(*elem));
  elem->val = nbr;
  elem->next = *l_a;
  *l_a = elem;
}

int	main(int argc, char **argv)
{
  int		i;
  int		*tab;
  t_l_a		*l_a;
  t_l_a		*l_b;

  i = argc - 1;
  l_a = NULL;
  if (argc == 1)
    {
      printf("[Error] : missing arguments\n");
      return (0);
    }
  while (i >= 1)
    {
      my_put_in_list(&l_a, atoi(argv[i]));
      i--;
    }
  sa(l_a);
  while (l_a != NULL)
    {
      printf("->%d\n", l_a->val);
      l_a = l_a->next;
    }
}
